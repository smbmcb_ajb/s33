// Create a fetch request using the GET method that will retrieve all the
// to do list items from JSON Placeholder API.


fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => console.log(data))


// Using the data retrieved, create an array using the map method to
// return just the title of every item and print the result in the console.
let arrayData = [];

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => {
	data.map((item)=> {
		arrayData.push(item.title)

	})
})
console.log(arrayData)



// Create a fetch request using the GET method that will retrieve a
// single to do list item from JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((data) => console.log(data))

// Using the data retrieved, print a message in the console that will
// provide the title and status of the to do list item.
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => console.log("status: " + response.status))

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())

.then((data) => {
	
	console.log("title: " + data.title)
	console.log("status: " + data.completed)
})

// Create a fetch request using the POST method that will create a to
// do list item using the JSON Placeholder API.



fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({

		userId: 1,
		title: 'New Post'
	})
})
.then((response1) => response1.json())
.then((data1) => console.log(data1))


// Create a fetch request using the PUT method that will update a to do
// list item using the JSON Placeholder API.

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Edited Post',
		
	})
})
.then((response2) => response2.json())
.then((data2) => console.log(data2))


// Update a to do list item by changing the data structure to contain
// the following properties:
// a. Title
// b. Description
// c. Status
// d. Date Completed
// e. User ID

const date = new Date()
let day = date.getDate()
let month = date.getMonth() + 1;
let year = date.getFullYear();
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Edited new Post',
		"description": 'added this description',
		// "status": 'added status',
		'date completed': `${day}/${month}/${year}`

		
	})
})
.then((response3) => response3.json())
.then((data3) => console.log(data3))


// Create a fetch request using the PATCH method that will update a to
// do list item using the JSON Placeholder API


fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Edited Post using Patch',
		
	})
})
.then((response4) => response4.json())
.then((data4) => console.log(data4))

// Update a to do list item by changing the status to complete and add
// a date when the status was changed.


fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Edited new Post change to completed',
		completed: true,
		'date status changed': `${day}/${month}/${year}`

		
	})
})
.then((response5) => response5.json())
.then((data5) => console.log(data5))




fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method: 'DELETE'
})

